#!/bin/bash

function logHadoopNodeRole() {
    echo "Role of Hadoop node: $DOCKER_HADOOP_NODE_ROLE"
}

function logHadoopSlaveNodes() {
    echo "Hadoop slave nodes: $DOCKER_HADOOP_SLAVES"
}

function overrideMesosHostname() {
    hostname $HADOOP_HOSTNAME
}

function addHadoopToSharedApps() {
    symbolic
    if ! [ -L '/mnt/shared/apps/hadoop' ]
    then
        ln -sf /usr/local/hadoop /mnt/shared/apps/hadoop
    fi
}

function checkIfIAmMasterNode() {
    if [[ $DOCKER_HADOOP_NODE_ROLE == "MASTER" ]]; then
        return 0
    fi
    return 1
}

function waitForHostnameToBeAvailableThroughDns() {
    echo "Checking if $HADOOP_HOSTNAME node is available"
    while ping -c 1 $HADOOP_HOSTNAME 2>&1 | grep -i -q "unknown host"
    do
        echo "Waiting 5s for $HADOOP_HOSTNAME node to be available"
        sleep 5
    done
    echo "Node $HADOOP_HOSTNAME is available"
}

function waitForSlaveNodesToBeAvailableThroughDns() {
    echo "Waiting for other HADOOP nodes to be available through DNS"
    source $HOME/constants
    for slave in $slaves
    do
        echo "Checking if $slave node is available"
        while ping -c 1 $slave 2>&1 | grep -i -q "unknown host"
        do
            echo "Waiting 5s for $slave node to be available"
            sleep 5
        done
        echo "Node $slave is available"
    done
}

function startSshService() {
    echo "Setting SSH server port to $SSH_PORT_OVERLAY"
    sed -i 's@Port 22@'"Port $SSH_PORT_OVERLAY"'@g' /etc/ssh/sshd_config
    echo "Starting SSH server"
    service ssh start
}

function interpolateHadoopConfigFiles() {
    echo "Interpolation of HADOOP config files"
    sed -i 's@|||DOCKER_HADOOP_MASTER|||@'"$DOCKER_HADOOP_MASTER"'@g' $HADOOP_HOME/etc/hadoop/core-site.xml
    sed -i 's@|||DOCKER_HADOOP_MASTER|||@'"$DOCKER_HADOOP_MASTER"'@g' $HADOOP_HOME/etc/hadoop/hdfs-site.xml
    sed -i 's@|||DOCKER_HADOOP_MASTER|||@'"$DOCKER_HADOOP_MASTER"'@g' $HADOOP_HOME/etc/hadoop/yarn-site.xml
    sed -i 's@|||DOCKER_HADOOP_MASTER|||@'"$DOCKER_HADOOP_MASTER"'@g' $HADOOP_HOME/etc/hadoop/mapred-site.xml
}

function formatNameNode() {
    /usr/local/hadoop/bin/hdfs namenode -format
}

function populateKnownHosts() {
    echo "Adding 0.0.0.0 to $HOME/.ssh/known_hosts"
    ssh-keyscan -p $SSH_PORT_OVERLAY -t rsa 0.0.0.0 >> /root/.ssh/known_hosts

    for slave in $slaves
    do
        echo "Adding $slave to $HOME/.ssh/known_hosts"
        ssh-keyscan -p $SSH_PORT_OVERLAY -t rsa $slave >> /root/.ssh/known_hosts
    done
}

function populateHadoopSlavesConfigFile() {
    source $HOME/constants

    echo "Clearing $HADOOP_HOME/etc/hadoop/slaves config file"
    echo '' > $HADOOP_HOME/etc/hadoop/slaves

    for slave in $slaves
    do
        echo "Adding $slave to $HADOOP_HOME/etc/hadoop/slaves"
        echo $slave >> $HADOOP_HOME/etc/hadoop/slaves
    done
}

function startHadoopInAllNodes() {
    echo "Starting DFS"
    $HADOOP_HOME/sbin/start-dfs.sh
    echo "Starting  YARN"
    $HADOOP_HOME/sbin/start-yarn.sh
}

function runIndefinitely() {
    tail -f /dev/null
}

overrideMesosHostname
addHadoopToSharedApps
startSshService

logHadoopNodeRole
logHadoopSlaveNodes
interpolateHadoopConfigFiles
waitForHostnameToBeAvailableThroughDns
formatNameNode

if checkIfIAmMasterNode
then
    waitForSlaveNodesToBeAvailableThroughDns
    populateKnownHosts
    populateHadoopSlavesConfigFile
    startHadoopInAllNodes
fi

runIndefinitely