FROM ubuntu:16.04

WORKDIR /root

RUN apt-get update && apt-get install -y openssh-server openjdk-8-jdk wget net-tools iputils-ping nmap telnet vim

RUN wget http://www-eu.apache.org/dist/hadoop/common/hadoop-2.9.1/hadoop-2.9.1.tar.gz && \
    tar -xzvf hadoop-2.9.1.tar.gz && \
    mv hadoop-2.9.1 /usr/local/hadoop && \
    rm hadoop-2.9.1.tar.gz

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ENV HADOOP_HOME=/usr/local/hadoop
ENV PATH=$PATH:$HADOOP_HOME/bin:/usr/local/hadoop/sbin

RUN printf '%s\n%s\n' "export JAVA_HOME=$JAVA_HOME" "$(cat /root/.bashrc)" > /root/.bashrc

ADD constants /root/constants
ADD entrypoint.sh /entrypoint.sh
ADD ssh/hadoop_rsa /root/.ssh/
ADD ssh/hadoop_rsa.pub /root/.ssh/
ADD ssh/ssh-config /root/.ssh/config
ADD hadoop-config/core-site.xml   /usr/local/hadoop/etc/hadoop/core-site.xml
ADD hadoop-config/hdfs-site.xml   /usr/local/hadoop/etc/hadoop/hdfs-site.xml
ADD hadoop-config/yarn-site.xml   /usr/local/hadoop/etc/hadoop/yarn-site.xml
ADD hadoop-config/mapred-site.xml /usr/local/hadoop/etc/hadoop/mapred-site.xml

RUN cat /root/.ssh/hadoop_rsa.pub >> /root/.ssh/authorized_keys
RUN chmod 0600 /root/.ssh/hadoop_rsa

RUN mkdir -p /root/tmp /root/hadoop-data/hdfs/namenode /root/hadoop-data/hdfs/datanode

WORKDIR $HADOOP_HOME

VOLUME $HADOOP_HOME

CMD /entrypoint.sh